#include <stdio.h>

/* function declaration */
void func(void);

static int count = 5; /* global variable */

int _count;
extern void write_extern();

int main() {
    {
        int mount;
        auto int month;
    }

    {
        register int  miles;
    }

    while(count--) {
        func();
    }

    _count = 5;
    write_extern();

    return 0;
}

/* function definition */
void func( void ) {
    static int i = 5; /* local static variable */
    i++;

    printf("i is %d and count is %d\n", i, count);
}
