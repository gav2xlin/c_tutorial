#include <stdio.h>

int main () {
    {
        /* local variable definition */
        int a = 10;

        /* while loop execution */
        while( a < 20 ) {
            printf("value of a: %d\n", a);
            a++;
        }
    }

    {
        int a;

        /* for loop execution */
        for( a = 10; a < 20; a = a + 1 ){
            printf("value of a: %d\n", a);
        }
    }

    {
        /* local variable definition */
        int a = 10;

        /* do loop execution */
        do {
            printf("value of a: %d\n", a);
            a = a + 1;
        } while( a < 20 );
    }

    {
        /* local variable definition */
        int i, j;

        for(i = 2; i<100; i++) {

            for(j = 2; j <= (i/j); j++)
                if(!(i%j)) break; // if factor found, not prime
            if(j > (i/j)) printf("%d is prime\n", i);
        }
    }

    {
        /* local variable definition */
        int a = 10;

        /* while loop execution */
        while( a < 20 ) {

            printf("value of a: %d\n", a);
            a++;

            if( a > 15) {
                /* terminate the loop using break statement */
                break;
            }
        }
    }

    {
        /* local variable definition */
        int a = 10;

        /* do loop execution */
        do {

            if( a == 15) {
                /* skip the iteration */
                a = a + 1;
                continue;
            }

            printf("value of a: %d\n", a);
            a++;

        } while( a < 20 );
    }

    {
        /* local variable definition */
        int a = 10;

        /* do loop execution */
        LOOP: do {

                if( a == 15) {
                    /* skip the iteration */
                    a = a + 1;
                    goto LOOP;
                }

                printf("value of a: %d\n", a);
                a++;

            } while( a < 20 );
    }

    for( ; ; ) {
        printf("This loop will run forever.\n");
    }

    return 0;
}
