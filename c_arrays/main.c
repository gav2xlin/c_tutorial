#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/* function declaration */
double getAverage(int arr[], int size);

int * getRandom( );

int main () {
    {
        int n[ 10 ]; /* n is an array of 10 integers */
        int i,j;

        /* initialize elements of array n to 0 */
        for ( i = 0; i < 10; i++ ) {
            n[ i ] = i + 100; /* set element at location i to i + 100 */
        }

        /* output each array element's value */
        for (j = 0; j < 10; j++ ) {
            printf("Element[%d] = %d\n", j, n[j] );
        }
    }

    {
        /* an array with 5 rows and 2 columns*/
        // int a[5][2] = { {0,0}, {1,2}, {2,4}, {3,6},{4,8}};
        int a[5][2] = {0, 0, 1, 2, 2, 4, 3, 6, 4, 8};
        int i, j;

        /* output each array element's value */
        for ( i = 0; i < 5; i++ ) {

            for ( j = 0; j < 2; j++ ) {
                printf("a[%d][%d] = %d\n", i,j, a[i][j] );
            }
        }
    }

    {
        /* an int array with 5 elements */
        int balance[5] = {1000, 2, 3, 17, 50};
        double avg;

        /* pass pointer to the array as an argument */
        avg = getAverage( balance, 5 ) ;

        /* output the returned value */
        printf( "Average value is: %f ", avg );
    }

    {
        /* a pointer to an int */
        int *p;
        int i;

        p = getRandom();

        for ( i = 0; i < 10; i++ ) {
            printf( "*(p + %d) : %d\n", i, *(p + i));
        }
    }

    {
        /* an array with 5 elements */
        double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
        double *p;
        int i;

        p = balance;

        /* output each array element's value */
        printf( "Array values using pointer\n");

        for ( i = 0; i < 5; i++ ) {
            printf("*(p + %d) : %f\n",  i, *(p + i) );
        }

        printf( "Array values using balance as address\n");

        for ( i = 0; i < 5; i++ ) {
            printf("*(balance + %d) : %f\n",  i, *(balance + i) );
        }
    }

    return 0;
}

// double getAverage(int arr[], int size) {
// double getAverage(int* arr, int size) {
double getAverage(int arr[5], int size) {

    int i;
    double avg;
    double sum = 0;

    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }

    avg = sum / size;

    return avg;
}

/* function to generate and return random numbers */
int * getRandom( ) {

    static int r[10];
    int i;

    /* set the seed */
    srand( (unsigned)time( NULL ) );

    for ( i = 0; i < 10; ++i) {
        r[i] = rand();
        printf( "r[%d] = %d\n", i, r[i]);
    }

    return r;
}
