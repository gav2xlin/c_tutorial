cmake_minimum_required(VERSION 3.5)

project(c_type_casting LANGUAGES C)

add_executable(c_type_casting main.c)

install(TARGETS c_type_casting
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
