cmake_minimum_required(VERSION 3.5)

project(c_recursion LANGUAGES C)

add_executable(c_recursion main.c)

install(TARGETS c_recursion
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
