cmake_minimum_required(VERSION 3.5)

project(c_input_output LANGUAGES C)

add_executable(c_input_output main.c)

install(TARGETS c_input_output
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
