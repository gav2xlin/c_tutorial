#include <stdio.h>
#include <time.h>
#include <stdlib.h>

const int MAX = 3;

void getSeconds(unsigned long *par);

/* function declaration */
double getAverage(int *arr, int size);

int * getRandom( );

int main () {
    {
        int  var1;
        char var2[10];

        printf("Address of var1 variable: %x\n", &var1  );
        printf("Address of var2 variable: %x\n", &var2  );
    }

    {
        int  var = 20;   /* actual variable declaration */
        int  *ip;        /* pointer variable declaration */

        ip = &var;  /* store address of var in pointer variable*/

        printf("Address of var variable: %x\n", &var  );

        /* address stored in pointer variable */
        printf("Address stored in ip variable: %x\n", ip );

        /* access the value using the pointer */
        printf("Value of *ip variable: %d\n", *ip );
    }

    {
        int  *ptr = NULL;

        printf("The value of ptr is : %x\n", ptr  );
    }

    {
        int  var[] = {10, 100, 200};
        int  i, *ptr;

        /* let us have array address in pointer */
        ptr = var;

        for ( i = 0; i < MAX; i++) {

            printf("Address of var[%d] = %x\n", i, ptr );
            printf("Value of var[%d] = %d\n", i, *ptr );

            /* move to the next location */
            ptr++;
        }
    }

    {
        int  var[] = {10, 100, 200};
        int  i, *ptr;

        /* let us have array address in pointer */
        ptr = &var[MAX-1];

        for ( i = MAX; i > 0; i--) {

            printf("Address of var[%d] = %x\n", i-1, ptr );
            printf("Value of var[%d] = %d\n", i-1, *ptr );

            /* move to the previous location */
            ptr--;
        }
    }

    {
        int  var[] = {10, 100, 200};
        int  i, *ptr;

        /* let us have address of the first element in pointer */
        ptr = var;
        i = 0;

        while ( ptr <= &var[MAX - 1] ) {

            printf("Address of var[%d] = %x\n", i, ptr );
            printf("Value of var[%d] = %d\n", i, *ptr );

            /* point to the next location */
            ptr++;
            i++;
        }
    }

    {
        int  var[] = {10, 100, 200};
        int i;

        for (i = 0; i < MAX; i++) {
            printf("Value of var[%d] = %d\n", i, var[i] );
        }
    }

    {
        int  var[] = {10, 100, 200};
        int i, *ptr[MAX];

        for ( i = 0; i < MAX; i++) {
            ptr[i] = &var[i]; /* assign the address of integer. */
        }

        for ( i = 0; i < MAX; i++) {
            printf("Value of var[%d] = %d\n", i, *ptr[i] );
        }
    }

    {
        const int MAX = 4;

        char *names[] = {
            "Zara Ali",
            "Hina Ali",
            "Nuha Ali",
            "Sara Ali"
        };

        int i = 0;

        for ( i = 0; i < MAX; i++) {
            printf("Value of names[%d] = %s\n", i, names[i] );
        }
    }

    {
        int  var;
        int  *ptr;
        int  **pptr;

        var = 3000;

        /* take the address of var */
        ptr = &var;

        /* take the address of ptr using address of operator & */
        pptr = &ptr;

        /* take the value using pptr */
        printf("Value of var = %d\n", var );
        printf("Value available at *ptr = %d\n", *ptr );
        printf("Value available at **pptr = %d\n", **pptr);
    }

    {
        unsigned long sec;
        getSeconds( &sec );

        /* print the actual value */
        printf("Number of seconds: %ld\n", sec );
    }

    {
        /* an int array with 5 elements */
        int balance[5] = {1000, 2, 3, 17, 50};
        double avg;

        /* pass pointer to the array as an argument */
        avg = getAverage( balance, 5 ) ;

        /* output the returned value  */
        printf("Average value is: %f\n", avg );
    }

    {
        /* a pointer to an int */
        int *p;
        int i;

        p = getRandom();

        for ( i = 0; i < 10; i++ ) {
            printf("*(p + [%d]) : %d\n", i, *(p + i) );
        }
    }

    return 0;
}

void getSeconds(unsigned long *par) {
    /* get the current number of seconds */
    *par = time( NULL );
    return;
}

double getAverage(int *arr, int size) {

    int  i, sum = 0;
    double avg;

    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }

    avg = (double)sum / size;
    return avg;
}

int * getRandom( ) {

    static int  r[10];
    int i;

    /* set the seed */
    srand( (unsigned)time( NULL ) );

    for ( i = 0; i < 10; ++i) {
        r[i] = rand();
        printf("%d\n", r[i] );
    }

    return r;
}
