#include <stdio.h>

/* function declaration */
int max(int num1, int num2);

/* function declaration */
void swap(int x, int y);

void _swap(int *x, int *y);

int main () {
    {
        /* local variable definition */
        int a = 100;
        int b = 200;
        int ret;

        /* calling a function to get max value */
        ret = max(a, b);

        printf( "Max value is : %d\n", ret );
    }

    {
        /* local variable definition */
        int a = 100;
        int b = 200;

        printf("Before swap, value of a : %d\n", a );
        printf("Before swap, value of b : %d\n", b );

        /* calling a function to swap the values */
        swap(a, b);

        printf("After swap, value of a : %d\n", a );
        printf("After swap, value of b : %d\n", b );
    }

    {
        /* local variable definition */
        int a = 100;
        int b = 200;

        printf("Before swap, value of a : %d\n", a );
        printf("Before swap, value of b : %d\n", b );

        /* calling a function to swap the values */
        _swap(&a, &b);

        printf("After swap, value of a : %d\n", a );
        printf("After swap, value of b : %d\n", b );
    }

    return 0;
}

/* function returning the max between two numbers */
int max(int num1, int num2) {

    /* local variable declaration */
    int result;

    if (num1 > num2)
        result = num1;
    else
        result = num2;

    return result;
}

void swap(int x, int y) {

    int temp;

    temp = x; /* save the value of x */
    x = y;    /* put y into x */
    y = temp; /* put temp into y */
}

void _swap(int *x, int *y) {

    int temp;

    temp = *x; /* save the value of x */
    *x = *y;    /* put y into x */
    *y = temp; /* put temp into y */
}
