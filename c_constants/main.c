#include <stdio.h>

#define LENGTH 10
#define WIDTH  5
#define NEWLINE '\n'

int main() {
    printf("Hello\tWorld\n\n");

    int area;

    area = LENGTH * WIDTH;
    printf("value of area : %d", area);
    printf("%c", NEWLINE);

    const int  _LENGTH = 10;
    const int  _WIDTH = 5;
    const char _NEWLINE = '\n';

    area = _LENGTH * _WIDTH;
    printf("value of area : %d", area);
    printf("%c", _NEWLINE);

    return 0;
}
