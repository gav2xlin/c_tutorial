cmake_minimum_required(VERSION 3.5)

project(c_file_io LANGUAGES C)

add_executable(c_file_io main.c)

install(TARGETS c_file_io
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
