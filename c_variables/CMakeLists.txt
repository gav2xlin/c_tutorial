cmake_minimum_required(VERSION 3.5)

project(c_variables LANGUAGES C)

add_executable(c_variables main.c)

install(TARGETS c_variables
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
