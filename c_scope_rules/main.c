#include <stdio.h>

/* global variable declaration */
int g;

/* global variable declaration */
int g = 20;

/* global variable declaration */
int a = 20;

int sum(int a, int b);

int main () {
    {
        /* local variable declaration */
        int a, b;
        int c;

        /* actual initialization */
        a = 10;
        b = 20;
        c = a + b;

        printf ("value of a = %d, b = %d and c = %d\n", a, b, c);
    }

    {
        /* local variable declaration */
        int a, b;

        /* actual initialization */
        a = 10;
        b = 20;
        g = a + b;

        printf ("value of a = %d, b = %d and g = %d\n", a, b, g);
    }

    {
        /* local variable declaration */
        int g = 10;

        printf ("value of g = %d\n",  g);
    }

    {
        /* local variable declaration in main function */
        int a = 10;
        int b = 20;
        int c = 0;

        printf ("value of a in main() = %d\n",  a);
        c = sum( a, b);
        printf ("value of c in main() = %d\n",  c);
    }

    return 0;
}

/* function to add two integers */
int sum(int a, int b) {

    printf ("value of a in sum() = %d\n",  a);
    printf ("value of b in sum() = %d\n",  b);

    return a + b;
}
